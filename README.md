ejabberd config file from schokokeks.org
========================================

We recently put some effort into enabling many modern XMPP features on our
ejabberd server at [schokokeks.org](https://schokokeks.org). We figured
out that it's often nontrivial to find out what option corresponds to what
XEP. Therefore we're sharing our config:

[ejabberd.yml](https://gitlab.com/hanno/ejabberd-config/blob/master/ejabberd.yml)

This config file enables:
* All options to get an "A" rating at the [IM Observatory](https://xmpp.net/).
* All XEPs that [Conversations](https://conversations.im/) asks for in its Account Details / Server Info.
* Most XEPs from the [XMPP Compliance Suites 2016 / XEP-0375](https://xmpp.org/extensions/xep-0375.html) (exception see below). (Also useful is the [Compliance Tester for XMPP Servers](https://github.com/iNPUTmice/ComplianceTester).)

In all cases we added a comment indicating the XEP number.

Broken PEP / XEP-0163
=====================

Sometimes Conversations complains that XEP-0163 is "broken". This can mean
that the pubsub database is corrupted due to an update from a previous
ejabberd version.

We successfully fixed this by deleting the pubsub database. A new database
gets automatically generated and clients usually re-upload previously
pushed pubsub data. Simply delete all `pubsub_*` files in the ejabberd spool
directory (usually `/var/spool/ejabberd`).

Push / XEP-0357
===============

Ejabberd currently contains no support for Push / XEP-0357, which is part
of the XMPP Compliance Suites 2016.

There is an [experimental push module on Github](https://github.com/royneary/mod_push),
but it hasn't seen any updates for quite a while and needs a specific
branch of ejabberd. Therefore it is currently not easily possible to get
Push support in ejabberd.
